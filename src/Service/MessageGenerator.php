<?php

// src/Service/MessageGenerator.php
namespace App\Service;

class MessageGenerator
{
    public function getHappyMessage(): string
    {
        $messages = [
            'Oh Year Tu l\'a fait',
            'Ce genre de Delire',
            'Super travail !!!!',
        ];

        $index = array_rand($messages);

        return $messages[$index];
    }
}
