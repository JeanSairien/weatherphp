<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use App\Service\MessageGenerator;
use App\Service\RequestClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;


class CityController extends AbstractController
{
    /**
    * @Route("/city/{city}", methods={"GET","HEAD"})
    */
    /*Fonction qui execute les requetes vers les apis meteos*/
    public function getCity(string $city, MessageGenerator $messageGenerator, LoggerInterface $logger): Response
    {
      if (!empty($city)) { // Si le paramettre de ville n'est pas vide alors on passe a la requete
        $client = HttpClient::create();
        // requetes openweathermap
        $response = $client->request('GET', 'https://api.openweathermap.org/data/2.5/weather?q='.$city.'&appid=984ef88c2770dd85f65151ddf9180083&language=fr&offset=1&units=metric');
        //  https://api.openweathermap.org/data/2.5/weather?q=London&appid={API key}

        $statusCode = $response->getStatusCode();

        if($statusCode === 404){ // si la ville soumise n'est pas trouvé par la premiere requete une autre requette est éxecuté par une autre api
          echo ("your request for ".$city." has not success with only Name 404 / 1"); // message envoyer si la premiere requette echoue

          $find = $client->request('GET', 'http://dataservice.accuweather.com/locations/v1/cities/search?apikey=<APIKEY>&q='.$city);
          $statusCode = $find->getStatusCode();
          $contentType = $find->getHeaders()['content-type'][0];
          $contentfind = $find->getContent();
          $contentfind = $find->toArray();


          var_dump("Switching to lat/long request 404 / 2"); // message envoyer lors du passage au requete par coordonnées
          $lat = $contentfind[0]["GeoPosition"]["Latitude"]; // quand la requete aboutit, nous récuperon la latitude
          $long = $contentfind[0]["GeoPosition"]["Longitude"];// ainsi que la longitude
          // grace a la <lat> et <long> recu lors de la précédente requete nous pouvons recommencer la premiere requette cette fois si avec la <lat> et la <long>
          $response = $client->request('GET', 'https://api.openweathermap.org/data/2.5/weather?lat='.$lat.'&lon='.$long.'&appid=<APPID>&language=fr&units=metric');
          $statusCode = $find->getStatusCode();
          $contentType = $find->getHeaders()['content-type'][0];
          $content = $find->getContent();
          $content = $find->toArray();
        }

        $contentType = $response->getHeaders()['content-type'][0];
        $content = $response->getContent();
        $content = $response->toArray();


        // variable a afficher dans le front aprés les requettes
         $lat = $content["coord"]["lat"];
         $long = $content["coord"]["lon"];
         $skystate = $content["weather"][0]["main"];
         $power = $content["weather"][0]["description"];
         $humidity = $content["main"]["humidity"];
         $temperature = $content["main"]["temp"];

        // var_dump($content);

      }

      // portée des variables dans le twig
      return $this->render('city.html.twig',[
        "city" => $city,
        "lat" => $lat,
        "long" => $long,
        "sky" => $skystate,
        "hum" => $humidity,
        "power" => $power,
        "temp" => $temperature,

        $message = $messageGenerator->getHappyMessage(),
        $this->addFlash('success', $message),
        $logger->info('You get success INT')

      ]);

    }

    /**
    * @Route("/city/nice")
    */
    // generation d'un nombre aleatoire
    public function list(LoggerInterface $logger, MessageGenerator $messageGenerator): Response
    {
      $number = random_int(0, 100);
      $message = $messageGenerator->getHappyMessage();
      return $this->render('city.html.twig',[
        'number' => $number,
        'city' => "Your city was here!",
        $logger->info('Look, I just used a service!'),
        $this->addFlash('success', $message)

      ]);

    }

}
