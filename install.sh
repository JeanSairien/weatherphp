#!/bin/sh
#extension ctype iconv ....ect ect:

sudo apt install php8.1-common


#composer
#get it
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

#move it for globale composer
sudo mv composer.phar /usr/local/bin/composer

# install dépendances
# composer create-project symfony/skeleton meteopy (not required)
composer install
composer require symfony/console
composer require symfony/console
composer require annotations
composer require annotations
composer require twig
composer recipes
composer require symfony/monolog-bundle
composer require symfony/monolog-bundle
composer require symfony/http-client
composer require symfony/asset
composer require symfony/webpack-encore-bundle

## install symfony cli
echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | sudo tee /etc/apt/sources.list.d/symfony-cli.list
sudo apt update
sudo apt install symfony-cli

## install yarn
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null

echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

apt update

apt install yarn
